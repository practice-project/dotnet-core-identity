﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Basic.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [Authorize]
        public IActionResult Secret()
        {
            return View();
        }
        public IActionResult Authenticate()
        {
            var myClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, "shuvo"),
                new Claim(ClaimTypes.Email, "shuvo@outlook.com"),
                new Claim("me.says", "I am rookie"),
            };

            var myIdentity = new ClaimsIdentity(myClaims, "myfuckingclaims")
            return RedirectToAction("Index");
        }
    }
}